#include "Wektor.hh"
using namespace std;
// przeciazenie operatora wyjscia
std::ostream &operator<<(std::ostream &strm, const Wektor &wektor)
{
  for (int i = 0; i < ROZMIAR; i++)
  {
    strm << wektor[i] << " ";
  }
  strm << endl;
  return strm;
}
// przeciazenie operatora wejscia
std::istream &operator>>(std::istream &Strm, Wektor &Wek)
{
  for (int i = 0; i < ROZMIAR; i++)
  {
    Strm >> Wek[i];
  }

  return Strm;
}
// przeciazenie odejmowania
Wektor operator-(Wektor pom, Wektor tmp)
{
  Wektor wynik;
  for (int i = 0; i < ROZMIAR; i++)
  {
    wynik[i] = pom[i] - tmp[i];
  }
  return wynik;
}
// przeciazenie dodawania
Wektor operator+(Wektor pom, Wektor tmp)
{
  Wektor wynik;
  for (int i = 0; i < ROZMIAR; i++)
  {
    wynik[i] = pom[i] + tmp[i];
  }
  return wynik;
}
// przeciazenie mnozenia wektora razy liczbe
Wektor operator*(float zmienna, Wektor tmp)
{
  Wektor wynik;
  for (int i = 0; i < ROZMIAR; i++)
  {
    wynik[i] = zmienna * tmp[i];
  }
  return wynik;
}
// przeciazenie mnozenia wektora * wektor
float operator*(Wektor pom, Wektor tmp)
{
  float k = 0, j = 0;
  for (int i = 0; i < ROZMIAR; i++)
  {
    j = pom[i] * tmp[i];
    k = k + j;
  }
  return k;
}
// przeciazenie dzielenia wektora przez liczbe
Wektor operator/(Wektor tmp,float zmienna)
{
  Wektor wynik;
  for (int i = 0; i < ROZMIAR; i++)
  {
    wynik[i] = tmp[i] / zmienna;
  }
  return wynik;
}
// funkcja wyliczajaca dlugosc wektora
float dlugoscwektora(Wektor blad)
{
  float wartoscbledu;
  wartoscbledu = blad * blad;
  wartoscbledu = sqrt(wartoscbledu);
  return wartoscbledu;
}
// funkcja liczaca iloczyn wektorowy *MODYFIKACJA*
Wektor Wektor::iloczynwektorowy(Wektor tmp, Wektor pom)
{
  Wektor iloczyn;
  iloczyn[0] = tmp[1] * pom[2] - tmp[2] * pom[1];
  iloczyn[1] = tmp[2] * pom[0] - tmp[0] * pom[2];
  iloczyn[2] = tmp[0] * pom[1] - tmp[1] * pom[0];

  return iloczyn;
}