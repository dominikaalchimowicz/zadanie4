#include "Macierz.hh"
// przeciazenie operatora wyjscia
std::ostream &operator<<(std::ostream &out, const Macierz &macierz)
{
    for (int i = 0; i < ROZMIAR; i++)
    {
        for (int j = 0; j < ROZMIAR; j++)
        {
            cout << macierz[i][j] << " ";
        }
        cout << endl;
    }
    return out;
}
// przeciazenie operatora wejscia
std::istream &operator>>(std::istream &in, Macierz &macierz)
{
    for (int i = 0; i < ROZMIAR; i++)
    {
        for (int j = 0; j < ROZMIAR; j++)
        {
            in >> macierz[i][j];
        }
    }
    return in;
}

// metoda wyliczenia wyznacznika
float Macierz::wyznacznik()
{
    float x = macierz[0][0] * macierz[1][1] * macierz[2][2];
    float y = macierz[0][1] * macierz[1][2] * macierz[2][0];
    float z = macierz[0][2] * macierz[1][0] * macierz[2][1];
    float k = x + y + z;
    x = macierz[2][0] * macierz[1][1] * macierz[0][2];
    y = macierz[2][1] * macierz[1][2] * macierz[0][0];
    z = macierz[2][2] * macierz[1][0] * macierz[0][1];
    float l = x + y + z;
    k = k - l;
    return k;
}
// funkcja zmiany kolejnosci kolumn w macierzy, w celu wyliczenia detX, detY, detZ, ktora kolumna zalezna od i
/* OZNACZENIA
Macierz macierz -   macierz ktorej zmieniamy kolumny
int i           -   zmienna sluzaca do okreslenia kolumny zmiany
Wektor wolnych  -   wektor wyrazow wolnych (nowa kolumna)
*/
Macierz zmianakolejnosci(Macierz macierz, int i, Wektor wolnych)
{
    for (int j = 0; j < ROZMIAR; j++)
    {
        macierz[i][j] = wolnych[j];
    }
    return macierz;
}
// metoda kopiująca wyrazy macierzy do innej macierzy
void Macierz::copy(Macierz from)
{
    for (int i = 0; i < ROZMIAR; i++)
    {
        for (int j = 0; j < ROZMIAR; j++)
        {
            macierz[i][j] = from[i][j];
        }
    }
}
// przeciazenie operatora mnozenia macierz*wektor
Wektor operator*(Macierz macierz, Wektor wektor)
{
    Wektor wynik;
    float b = 0;
    for (int i = 0; i < ROZMIAR; i++)
    {
        for (int j = 0; j < ROZMIAR; j++)
        {
            b = macierz[i][j] * wektor[j];
        }
        wynik[i] = b;
    }
    return wynik;
}
// funkcja wyliczania bledu rozwiazan
Wektor wyliczaniebledu(Macierz macierz, Wektor rozwiazania, Wektor wolne)
{
    Wektor wynik;
    wynik = macierz * rozwiazania; // przeciazenie operatora mnozenia (Macierz.cpp)
    wynik = wynik - wolne;

    return wynik;
}
// metoda podnoszenia wyrazow macierzy do kwadratu *MODYFIKACJA*
void Macierz::podnoszeniedokwadratu()
{
    for (int i = 0; i < ROZMIAR; i++)
    {
        for (int j = 0; j < ROZMIAR; j++)
        {
            macierz[i][j] *= macierz[i][j];
        }
    }
}

Macierz mnozeniemacierzyMOD(Macierz pom, Macierz tmp)
{
    Macierz wynik;
    float a,b;
    for (int i = 0; i < ROZMIAR; i++)
    {
        for (int j = 0; j < ROZMIAR; j++)
        {
            for (int k = 0; k < ROZMIAR; k++)
            {
                
                a = pom[i][k] * tmp[k][j];
                b = b + a;
            }
            wynik[i][j] = b;
            a = 0, b = 0;
        }
    }
    return wynik;
}

Macierz odejmowaniemacierzyMOD(Macierz pom, Macierz tmp)
{
    Macierz wynik;
    for (int i = 0; i < ROZMIAR; i++)
    {
        for (int j = 0; j < ROZMIAR; j++)
        {
            wynik[i][j] = pom[i][j] - tmp[i][j];
        }
    }
    return wynik;
}

void tworz(Macierz &pom, int k)
{
    for (int i = 0; i < ROZMIAR; i++)
    {
        for (int j = 0; j < ROZMIAR; j++)
        {
            pom[i][j] = i+j * k;
        }
    }
}