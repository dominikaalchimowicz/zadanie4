#include <iostream>
#include <math.h>
#include "Wektor.hh"
#include "Macierz.hh"
#include "UkladRownanLiniowych.hh"

using namespace std;

int main()
{
  // UkladRownanLiniowych uklad;           // tworzenie ukladu rownan
  // cin >> uklad;                         // wczytywanie ukladu rownan ze standardowego wejscia
  // cout << uklad << endl;                // wypisywanie ukladu rownan na standardowe wyjscie
  // uklad.rozwiaz();                      // rozwiazanie ukladu,  funkcja w pliku UkladRownanLiniowych.cpp
  // uklad.wyliczeniebledu();              // wyliczenie bledu,    funkcja w pliku UkladRownanLiniowych.cpp

  // /* rozpoczecie modyfikacji */
  // /* iloczyn wektorowy */
  // cout << " wyliczanie iloczynu wektorowego wprowadz wektory." << endl;
  // Wektor jeden, dwa;                    // tworzenie wektorow do iloczynu wektorowego
  // cin >> jeden;                         // wczytywanie
  // cin >> dwa;
  // Wektor iloczyn;
  // cout << "twoj wektor wynosi :   ";
  // cout << iloczyn.iloczynwektorowy(jeden, dwa); // wyliczenie iloczynu i wypisanie na cout, funkcja w pliku Wektor.cpp

  // /* macierz do kwadratu */
  // Macierz dokwadratu;                   // tworzenie macierzy
  // cout << " wyliczanie macierzy do kwadratu wprowadz macierz." << endl;
  // cin >> dokwadratu;                    // wczytywanie
  // dokwadratu.podnoszeniedokwadratu();   // wyliczenie funkcji, funkcja w pliku Macierz.cpp
  // cout << "twoja macierz wynosi :   \n";
  // cout << dokwadratu << endl;           // wypisanie na standardowe wyjscie 
Macierz macierz1;
Macierz macierz2;
Macierz odjeta;
Macierz mnozona;
tworz(macierz1,4);
cout << "macierz pierwsza, wyrazy tworzone wzorem i+j*4" << endl;
cout << macierz1 << endl;

tworz(macierz2,2);
cout << "macierz druga, wyrazy tworzone wzorem i+j*2" << endl;
cout << macierz2 << endl;
odjeta = odejmowaniemacierzyMOD(macierz1, macierz2);
cout << "macierz powstala z odejmowania macierzy pierwszej i drugiej" << endl;
cout << odjeta << endl;
mnozona = mnozeniemacierzyMOD(macierz1, macierz2);
cout << "macierz powstala z pomnozenia macierzy pierwszej i drugiej" << endl;
cout << mnozona << endl;



  return 0;
}