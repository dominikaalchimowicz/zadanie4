#include "UkladRownanLiniowych.hh"
// metoda rozwiazujaca uklad rownan
/* OZNACZENIA
Wektor  wyznaczniki -   wektor przechowujący wyznaczniki detX, detY, detZ 
                        (ściśle powiazany z funkcją zmianakolejności(Macierz.cpp))

float   Glowny      -   zmienna przechowujaca wartosc wyznacznika glownego macierzy 

Macierz Nowa        -   skopiowana macierz glowna sluzaca do liczenia wyznacznikow detX, detY, detZ 
                        (ściśle powiazany z funkcją zmianakolejności(Macierz.cpp) i copy(Macierz.cpp))
*/

int UkladRownanLiniowych::rozwiaz()
{
    Wektor wyznaczniki;                         // tworzenie wektora wyznacznikow

    float Glowny = 0;                           // zmienna wyznacznika glownego macierzy

    Glowny = macierz.wyznacznik();              // wyliczanie wyznacznika za pomoca metody(Macierz.cpp)
    if (Glowny == 0)
    {
        cout << "wyznacznik macierzy = 0, blad metody Cramera " << endl;
        return 0;
    }

    Macierz Nowa;                               // tworzenie macierzy pomocniczej
    for (int wiersz = 0; wiersz < ROZMIAR; wiersz++)
    {
        Nowa.copy(macierz);                     // skopiowanie macierzy glownej do macierzy pomocniczej
        Nowa = zmianakolejnosci(Nowa, wiersz, wektor); 
        wyznaczniki[wiersz] = Nowa.wyznacznik();
    }
    cout << endl;
    cout << "Rozwiazanie x = (x1, x2, x3): " << endl;
    for (int i = 0; i < ROZMIAR; i++)
    {
        rozwiazania[i] = wyznaczniki[i] / Glowny;   // wyliczenie rozwiazan X,Y,Z ze wzoru X = detX/Glowny
        cout << rozwiazania[i] << "  ";
    }
    cout << "\n"
         << endl;
    return 0;
}
// metoda wyliczajaca blad rozwiazania
/*OZNACZENIA:
Wektor  bladrozwiazan   -   wektor przechowujacy wektor bledu
float   wartoscbledu    -   zmienna przechowujaca wartosc dlugosci wektora bledu
*/
void UkladRownanLiniowych::wyliczeniebledu()
{
    cout << " \t Wektor bledu: Ax-b = ";
    Wektor bladrozwiazan;
    bladrozwiazan = wyliczaniebledu(macierz, rozwiazania, wektor); // w skrocie macierz_glowna*rozwiazania - wektor_wyrazow_wolnych, (Macierz.cpp)
    cout << bladrozwiazan << endl;

    /* dlugosc wektora*/
    cout << "Dlugosc wektora bledu: ||Ax-b||  = ";
    float wartoscbledu;
    wartoscbledu = dlugoscwektora(bladrozwiazan); //w skrocie sqrt(bladrozwiazan*bladrozwiazan), (Wektor.cpp)
    cout << wartoscbledu << endl;
}
/* OZNACZENIA DLA PRZECIAZEN:
UklRown.getmacierz()    -   funkcja dajaca dostep do pol prywatnych klasy
UklRown.getwektor()     -   funkcja dajaca dostep do pol prywatnych klasy
*/
// przeciazenie operatora wejscia
std::istream &operator>>(std::istream &Strm, UkladRownanLiniowych &UklRown)
{
    cin >> UklRown.getmacierz();
    cin >> UklRown.getwektor();
    return Strm;
}
// przeciazenie operatora wyjscia
std::ostream &operator<<(std::ostream &Strm, const UkladRownanLiniowych &UklRown)
{
    cout << "macierz glowna:" << endl;
    cout << UklRown.getmacierz() << endl;
    cout << "wektor wyrazow wolnych:" << endl;
    cout << UklRown.getwektor() << endl;
    return Strm;
}