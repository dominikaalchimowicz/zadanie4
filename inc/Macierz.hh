#ifndef MACIERZ_HH
#define MACIERZ_HH

#include "rozmiar.h"
#include "Wektor.hh"
#include <iostream>

class Macierz
{
  Wektor macierz[ROZMIAR];

public:
  // operatory indeksujace (dzieki nim mozna operowac na prywatnej czesci klasy)
  Wektor operator[](int Ind) const { return macierz[Ind]; }
  Wektor &operator[](int Ind) { return macierz[Ind]; }
  Macierz()
  {
    for (int i = 0; i < ROZMIAR; i++)
    {
      for (int j = 0; j < ROZMIAR; j++)
      {
        macierz[i][j] = 0;
      }
    }
  }
  // metody klasy macierzy
  float wyznacznik();
  void copy(Macierz from);
  void podnoszeniedokwadratu();
};

std::istream &operator>>(std::istream &in, Macierz &macierz);
std::ostream &operator<<(std::ostream &out, const Macierz &macierz);
Wektor operator*(Macierz macierz, Wektor wektor);
Wektor wyliczaniebledu(Macierz macierz, Wektor tmp, Wektor wolne);
Macierz zmianakolejnosci(Macierz macierz, int i, Wektor wolnych);
Macierz mnozeniemacierzyMOD(Macierz pom, Macierz tmp);
Macierz odejmowaniemacierzyMOD(Macierz pom, Macierz tmp);
void tworz(Macierz &pom, int k);


#endif