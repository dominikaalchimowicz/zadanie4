#ifndef WEKTOR_HH
#define WEKTOR_HH

#include "rozmiar.h"
#include <iostream>
#include <math.h>
using namespace std;

class Wektor
{
  float wektor[ROZMIAR];

public:
// operatory indeksujace (dzieki nim mozna operowac na prywatnej czesci klasy)
  float operator[](int Ind) const { return wektor[Ind]; }
  float &operator[](int Ind) { return wektor[Ind]; }

  void wypiszWektor()
  {
    for (int i = 0; i < ROZMIAR; i++)
    {
      cout << wektor[i] << " ";
    }
    cout << endl;
  }
  Wektor iloczynwektorowy(Wektor tmp, Wektor pom);
};
float dlugoscwektora(Wektor tmp);
float operator*(Wektor pom, Wektor tmp);
Wektor operator+(Wektor pom, Wektor tmp);
Wektor operator-(Wektor pom, Wektor tmp);
Wektor operator/(Wektor tmp,float zmienna);
Wektor operator*(float zmienna, Wektor tmp);
std::istream &operator>>(std::istream &Strm, Wektor &Wek);
std::ostream &operator<<(std::ostream &cout, const Wektor &Wek);

#endif
