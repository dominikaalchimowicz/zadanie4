#ifndef UKLADROWNANLINIOWYCH_HH
#define UKLADROWNANLINIOWYCH_HH

#include <iostream>
#include "Wektor.hh"
#include "Macierz.hh"

class UkladRownanLiniowych
{
  Macierz macierz;
  Wektor wektor;
  Wektor rozwiazania;

public:
  Macierz getmacierz() const { return macierz; }
  Macierz &getmacierz() { return macierz; }
  Wektor getwektor() const { return wektor; }
  Wektor &getwektor() { return wektor; }
// konstruktor argumentowy gdzie wpisywane sa argumenty odrazu do ukladu
  UkladRownanLiniowych(Macierz a, Wektor b)
  {
    for (int i = 0; i < ROZMIAR; i++)
    {
      for (int j = 0; j < ROZMIAR; j++)
      {
        macierz[i][j] = a[i][j];
      }
      wektor[i] = b[i];
      rozwiazania[i] = 0;
    }
  }
// konstruktor bezargumentowy zerujacy macierz i wektor wlasny
  UkladRownanLiniowych()
  {
    for (int i = 0; i < ROZMIAR; i++)
    {
      for (int j = 0; j < ROZMIAR; j++)
      {
        macierz[i][j] = 0;
      }
      wektor[i] = 0;
      rozwiazania[i] = 0;
    }
  }
// metody klasy ukladu rownan
  int rozwiaz();
  void wyliczeniebledu();
};

std::istream &operator>>(std::istream &Strm, UkladRownanLiniowych &UklRown);
std::ostream &operator<<(std::ostream &Strm, const UkladRownanLiniowych &UklRown);

#endif