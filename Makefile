TRGDIR=./
OBJ=./obj
FLAGS= -Wall -pedantic -std=c++14 -iquote inc

__start__: ${TRGDIR}/uklad_rownan

${TRGDIR}/uklad_rownan: ${OBJ} ${OBJ}/main.o ${OBJ}/UkladRownanLiniowych.o ${OBJ}/Macierz.o  ${OBJ}/Wektor.o
	g++ -o ${TRGDIR}/uklad_rownan ${OBJ}/main.o ${OBJ}/Macierz.o ${OBJ}/Wektor.o ${OBJ}/UkladRownanLiniowych.o

${OBJ}:
	mkdir ${OBJ}

${OBJ}/main.o: src/main.cpp inc/UkladRownanLiniowych.hh inc/Macierz.hh inc/Wektor.hh inc/rozmiar.h
	g++ -c ${FLAGS} -o ${OBJ}/main.o src/main.cpp

${OBJ}/UkladRownanLiniowych.o: src/UkladRownanLiniowych.cpp inc/UkladRownanLiniowych.hh
	g++ -c ${FLAGS} -o ${OBJ}/UkladRownanLiniowych.o src/UkladRownanLiniowych.cpp

${OBJ}/Macierz.o : src/Macierz.cpp inc/Macierz.hh
	g++ -c ${FLAGS} -o ${OBJ}/Macierz.o src/Macierz.cpp

${OBJ}/Wektor.o : src/Wektor.cpp inc/Wektor.hh
	g++ -c ${FLAGS} -o ${OBJ}/Wektor.o src/Wektor.cpp

clear:
	rm -f ${TRGDIR}/uklad_rownan ${OBJ}/*